import json 

class ContactManager():
    def __init__(self, fileName):
        self.fileName = fileName
        self.contactList = []
    def loadContact(self):
        fp=open(self.fileName)
        td=json.load(fp)
        for contact in td['contacts']:
            NewContact = {'name' : contact['name'], 'surname' : contact['surname'], 'mail': contact['mail']}
            self.contactList.append(NewContact)    
    def ShowContact(self):
        for contact in self.contactList:
            print(contact)

if __name__=="__main__":
    cm = ContactManager("contacts.json")
    cm.loadContact()
    cm.ShowContact
