import cherrypy
import json


class Device():
    def __init__(self, Id, name, measure, unit):
        self.deviceID= Id
        self.deviceName= name
        self.measureType= measure
        self.unit=unit
        
    def __repr__(self):
        return "deviceID:{},\ndeviceName:{},\nmeasureType:{},\nunit:{}".format(self.deviceID, self.deviceName, self.measureType, self.unit)
    
    def jsonify(self):
        device = {'deviceID': self.deviceID, 'deviceName': self.deviceName, 'measureType': self.measureType, 'unit': self.unit}
        return device

class Form():
    
    exposed=True
    
    def __init__(self):
        fp=open("devices.json", "r")
        json_file=json.load(fp)
        self.devices=[]
        self.json_string=[]
        for device in json_file.get('devicesList'):
            self.devices.append(Device(device.get('deviceID'),device.get('deviceName'),device.get('measureType'),device.get('unit')))
        fp.close()
    
    def GET(self):
        return open("index.html")
    
    def POST(self, deviceID, deviceName, measureType, unit):
        self.devices.append(Device(deviceID, deviceName, measureType, unit))
        content={'devicesList':[]}
        fp=open("devices.json", "r+")
        for d in self.devices:
            content['devicesList'].append(d.jsonify())
        self.json_string=json.dumps(content)
        json.dump(content, fp)
        fp.close()
#        raise cherrypy.HTTPRedirect('/')
        
        
        
if __name__=='__main__':
    conf = { 
        '/': { 
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(), 
             'tools.sessions.on': True, 
        } 
    } 
    cherrypy.tree.mount(Form(), '/', conf)
    
    cherrypy.engine.start()
    cherrypy.engine.block()