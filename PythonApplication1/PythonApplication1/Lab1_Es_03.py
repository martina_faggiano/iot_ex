import json


class Device():
    
    def __init__(self, Id, name, measure, services, details, date):
        self.deviceID= Id
        self.deviceName= name
        self.measureType= measure
        self.availableServices= services
        self.servicesDetails= details
        self.lastUpdate= date
    def __repr__(self):
        return "DeviceID:{},\nDeviceName:{},\nmeasureType:{},\navailableServices:{},\nservicesDetails:{},\nlastUpdate:{}\n".format(self.deviceID, self.deviceName, self.measureType, self.availableServices, self.servicesDetails, self.lastUpdate)
    
    def jsonify(self):
        device = {'deviceID': self.deviceID, 'deviceName': self.deviceName, 'measureType': self.measureType, 'availableServices': self.availableServices, 'servicesDetails': self.servicesDetails, 'lastUpdate': self.lastUpdate}
        return device
class ManageDevice():
    
    def __init__(self):
        json_file=json.load(open("catalog.json"))
        self.devices=[]
        for device in json_file.get('devicesList'):
            self.devices.append(Device(device.get('deviceID'),device.get('deviceName'),device.get('measureType'),device.get('availableServices'),device.get('servicesDetails'),device.get('lastUpdate')))
        
    def show(self):
        for device in self.devices:
            print(device)
    
    def searchByName(self, name):
        results=[device for device in self.devices if device.deviceName==(name)]
        print("I found the following results:\n")
        for x in results:
            print(x)
        
    def searchById(self, Id):
        results=[device for device in self.devices if device.deviceID==Id]
        print("I found the following results:\n")
        for x in results:
            print(x)
    
    def searchById1(self, Id):
        results=[device for device in self.devices if device.deviceID==Id]
        if(len(results)==1):
            return True
        else:
            return False
    
    def searchByService(self, service):
        results=[device for device in self.devices if device.availableServices.count(service)>0]
        print("I found the following results:\n")
        for x in results:
            print(x)
    
    def searchByMeasureType(self, measure):
        results=[device for device in self.devices if device.measureType.count(measure)>0]
        print("I found the following results:\n")
        for x in results:
            print(x)
    
    def insert(self, Id, name, measure, services, details, date):
        self.devices.append(Device(Id, name, measure, services, details, date))
        
    def update(self, Id, name, measure, services, details, date):
        results=[device for device in self.devices if device.deviceID==Id]
        self.devices.pop(self.devices.index(results[0]))
        self.devices.append(Device(Id, name, measure, services, details, date))
        
    def save(self):
        fp=open('catalog.json', 'w')
        content={'devicesList':[]}
        for d in self.devices:
            content['devicesList'].append(d.jsonify())
        json.dump(content, fp)
        fp.close


if __name__ == "__main__":
    
    manageDevice=ManageDevice()
    print('Welcome to the application to manage your devices')

    c=''
    helpMessage="Press 's' tho show the list of devices\nPress 'name' to search a device by name\nPress 'id' to search a device by id\nPress 'service' to search a device by service\nPress 'measure' to search a device by measureType\nPress 'insert' to create a new device\nPress 'q'to save end exit"
    
    while True:
        print(helpMessage)
        command=input()
        if command=='s':
            manageDevice.show()
        elif command=='name':
            l=input('Write the name of the device: ')
            manageDevice.searchByName(l)
        elif command=='id':
            l=input('Write the id of the device: ')
            manageDevice.searchById(l)
        elif command=='service':
            l=input('Write the service of the device: ')
            manageDevice.searchByService(l)
        elif command=='measure':
            l=input('Write the measure of the device: ')
            manageDevice.searchByMeasureType(l)
        elif command=='insert':
            ID=input('Write the id of the new device: ')
            name=input('Write the name of the new device: ')
            nMeasure=input('Write the number of MeasureType of the new device: ')
            MeasureType=[]
            for i in range(int(nMeasure)):
                measure=input('Write the measuretype of the new device: ')
                MeasureType.append(measure)
            nService=input('Write the number of availableServices of the new device: ')
            availableServices=[]
            for i in range(int(nService)):
                service=input('Write the availableService of the new device: ')
                availableServices.append(service)
            servicesDetails=[]
            for i in range(int(nService)):
                detail = {"serviceType": "",
                          "serviceIP": "",
                          "topic": []}
                Ip=input('Write the serviceIP of the service %s: ' % availableServices[i])
                nTopic=input('Write the number of the topic of the service %s: ' % availableServices[i])
                Topic=[]
                for j in range(int(nTopic)):
                    topic=input('Write the topic of the service %s: ' %availableServices[i])
                    Topic.append(topic)
                detail["serviceType"] = availableServices[i]
                detail["serviceIP"] = Ip
                detail["topic"] = Topic
                servicesDetails.append(detail)
            update=input('Write the date of the update of the service %s: ' %availableServices[i])
            if(manageDevice.searchById1(int(ID)):
                manageDevice.update(int(ID), name, MeasureType, availableServices, servicesDetails, update)
            else:
                manageDevice.insert(int(ID), name, MeasureType, availableServices, servicesDetails, update)
            print("The new device is created")
        elif command=='q':
            manageDevice.save()
            break
        else:
            print('Command not available')
