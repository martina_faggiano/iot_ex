import cherrypy

class Reverse(object):
    exposed= True
    def GET(self,*uri):
        output = 'write an uri'
        if len(uri)!=0:
            uri = ''.join(uri)[::-1]
            output = uri
        return output

if __name__=="__main__":
    conf = {
        '/':{
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
            }
        }
    #per cambiare la porta, amche se è meglio creare un file json di configurazione esterno
    #cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(Reverse(),'/', conf)