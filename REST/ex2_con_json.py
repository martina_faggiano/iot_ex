import cherrypy
import json

class PutJson():
    exposed = True
    def __init__(self, *args, **kwargs):
        pass

    def PUT(self):
        body = cherrypy.request.body.read() #the body is read as a string
        jsonBody = json.loads(body)# from string to dict 
        output = f" keys: {list(jsonBody.keys())}, values:{list(jsonBody.values())}" #list() forced to be a list
        return output
if __name__=="__main__":
    conf = {
        '/':{
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
            }
        }
    #per cambiare la porta, amche se è meglio creare un file json di configurazione esterno
    #cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(PutJson(),'/', conf)

