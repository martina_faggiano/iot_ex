import cherrypy

class something():
    exposed = True
    def __init__(self, *args, **kwargs):
        pass

    def GET(self,**params):
        output = "Hi!"
        if params!={}:
            keyList = list(params.keys())
            valueList = [params[key] for key in params.keys()]
            output = f" keys: {keyList}, values:{valueList}"
            return output
if __name__=="__main__":
    conf = {
        '/':{
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
            }
        }
    #per cambiare la porta, amche se è meglio creare un file json di configurazione esterno
    #cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(something(),'/', conf)
