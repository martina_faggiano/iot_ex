import cherrypy

class HelloWorld(object): 
    exposed = True
    def GET(self,*uri,**params):
        #standard output
        output = "Hello World"
        uri=list(uri)
        if len(uri)!=0 :
            output+='<br>uri: '+','.join(uri)
        if params!={}:
            output+='<br>params: '+str(params)
        return output
    
if __name__=="__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
            }
    }
    cherrypy.quickstart(HelloWorld(),'/', conf)